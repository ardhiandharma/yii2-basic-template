<?php

use yii\helpers\Json;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    //please change with unique id and name
    'id' => 'basic',
    'name' => 'Basic App',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'atHTmzslM07mVOALK_BF4lnPVa5b0F5r',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager'
        ],
        'authClientCollection' => [
            'class'   => \yii\authclient\Collection::className(),
            'clients' => [
                // please change clientId and clientSecret
                'sso' => [
                    'class' => 'app\components\KemenkeuClient',
                    'clientId' => 'your-client-id',
                    'clientSecret' => 'your-client-secret',
                    'issuerUrl' => 'https://sso.kemenkeu.go.id',
                    'scope' => 'openid profile profil.hris'
                ],
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@app/views/user'
                ],
            ],
        ],
        'session' => [
            //please change with unique name
            'name' => 'app_basic_yOTeYDFsqx',
        ],
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'site/*',
            'user/*',
            'admin/*', //please delete when superuser role is created
        ]
    ],
    'params' => $params,
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'admins' => ['administrator'],
            'enableConfirmation' => false,
            'enablePasswordRecovery' => false,
            'modelMap' => [
                'User' => 'app\models\User',
            ],
        ],
        'admin' => [
            'class' => 'mdm\admin\Module',
            'controllerMap' => [
                'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    'extraColumns' => [
                        [
                            'attribute' => 'name',
                            'label' => 'Name',
                            'value' => function ($model, $key, $index, $column) {
                                $name = '-';
                                if (!empty($model->accounts)) {
                                    $name = Json::decode($model->accounts['sso']->data)['name'];
                                }
                                return $name;
                            },
                        ],
                    ],
                ],
            ],
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
