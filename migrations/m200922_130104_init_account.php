<?php

use yii\db\Migration;

/**
 * Class m200922_130104_init_account
 */
class m200922_130104_init_account extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('user', [
            'id' => 1,
            'username' => 'administrator',
            'email' => 'ikp.bkf@kemenkeu.go.id',
            'password_hash' => '$2y$10$EMlavsOOYzfqIJZthWHGd.3zQH5flUQKeTus9waRAwRUNGhGSL0bC',
            'auth_key' => 'ti1HZOHNATUvZKnq0erXeMCE_lE0mPdp',
            'confirmed_at' => 1600779524,
            'unconfirmed_email' => null,
            'blocked_at' => null,
            'registration_ip' => null,
            'created_at' => 1600779524,
            'updated_at' => 1600779524,
            'flags' => 0,
            'last_login_at' => 1600779524
        ]);

        $this->insert('profile', ['user_id' => 1]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('user');
        $this->truncateTable('profile');
    }
}
