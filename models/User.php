<?php

namespace app\models;

use dektrium\user\models\User as BaseUser;
use yii\helpers\Json;

class User extends BaseUser
{
    public function getAvatar()
    {
        $account = $this->getAccountByProvider('sso');
        return Json::decode($account->data)['gravatar'];
    }

    public function getUnit()
    {
        $account = $this->getAccountByProvider('sso');
        $organisasi = Json::decode($account->data)['organisasi'];
        $arr_organisasi = explode(',', $organisasi);
        return sizeof($arr_organisasi) >= 2 ? $arr_organisasi[sizeof($arr_organisasi) - 2] : $arr_organisasi[sizeof($arr_organisasi) - 1];
    }

    public function getFullName()
    {
        $account = $this->getAccountByProvider('sso');
        return Json::decode($account->data)['name'];
    }
}
